//BEGIN DEFAULT CONFIG
var bowerPath = 'bower_components/';
var publicPath = 'public/';
var srcLessFile = 'src/less/main.less';
var finalCss = 'css/app.min.css';
var finalJs = 'js/app.min.js';
var mainTemplatePath = 'public/index.html';
var assetSourcePath = 'src/';
var vendorJsFiles = [ //Use only non minified versions here (because prod task do minify)
    'jquery/dist/jquery.js',
    'bootstrap/dist/js/bootstrap.js'
];
var projectJsFiles = [
    'src/js/app.js'
];
var vendorCssFiles = [ //Import vendor less files in your project less file(s)
    'animate.css/animate.css'
];
//END DEFAULT CONFIG


var commonConcatSrc = [];
for (var i = 0; i < vendorJsFiles.length; i++) {
    commonConcatSrc.push(bowerPath + vendorJsFiles[i]);
}
for (i = 0; i < projectJsFiles.length; i++) {
    commonConcatSrc.push(projectJsFiles[i]);
}

var lessCommonFiles = {};
lessCommonFiles[publicPath + finalCss] = srcLessFile;
var cssMinCommonFiles = {};
cssMinCommonFiles[publicPath + finalCss] = [];
for (i = 0; i < vendorCssFiles.length; i++) {
    cssMinCommonFiles[publicPath + finalCss].push(bowerPath + vendorCssFiles[i]);
}
cssMinCommonFiles[publicPath + finalCss].push(publicPath + finalCss);

var cacheBreakMatchAssets = {};
cacheBreakMatchAssets[finalJs] = publicPath + finalJs;
cacheBreakMatchAssets[finalCss] = publicPath + finalCss;

module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            common: {
                src: commonConcatSrc,
                dest: publicPath + finalJs
            }
        },
        removelogging: {
            prod: {
                src: publicPath + finalJs,
                dest: publicPath + finalJs
            }
        },
        uglify: {
            prod: {
                src: [publicPath + finalJs],
                dest: publicPath + finalJs
            }
        },

        less: {
            common: {
                options: {
                    paths: ['less'],
                    yuicompress: false
                },
                files: lessCommonFiles
            }
        },
        cssmin: {
            common: {
                files: cssMinCommonFiles
            }

        },
        copy: {
            vendorFonts: {
                expand: true,
                cwd: bowerPath,
                src: '**/fonts/**',
                dest: publicPath + 'fonts/',
                flatten: true,
                filter: 'isFile'
            },
            appFonts: {
                expand: true,
                cwd: assetSourcePath,
                src: '**/fonts/**',
                dest: publicPath + 'fonts/',
                flatten: true,
                filter: 'isFile'
            },
            images: {
                expand: true,
                cwd: assetSourcePath + 'images',
                src: '**/*',
                dest: publicPath + 'images'
            }
        },
        cachebreaker: {
            prod: {
                options: {
                    match: [cacheBreakMatchAssets],
                    replacement: 'md5'
                },
                files: {
                    src: [mainTemplatePath]
                }
            }
        },
        watch: {
            gruntFile: {
                files: 'Gruntfile.js',
                tasks: ['default']
            },
            js: {
                files: projectJsFiles,
                tasks: ['concat', 'jshint']
            },
            less: {
                files: assetSourcePath + '**/*.less',
                tasks: ['less', 'cssmin']
            },
            copy: {
                files: [assetSourcePath + '**/fonts/**', assetSourcePath + 'images/**'],
                tasks: ['copy']
            }
        },
        jshint: {
            all: projectJsFiles
        }
    });

    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-remove-logging');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-cache-breaker');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-jshint');

    grunt.registerTask('default', ['concat', 'less', 'cssmin', 'copy', 'jshint']);
    grunt.registerTask('prod', ['concat', 'removelogging', 'uglify', 'less', 'cssmin', 'copy', 'cachebreaker']);

};