#Grunt skeleton

##Install
Just run npm install, and you are ready to use this GruntFile

##Configuration
This Gruntfile is (try to be) smart, you don't need to dig deep, just fill the config the beginning of the Gruntfile,
run the tasks and enjoy the working Grunt. :)

##Example
This project not just a Gruntfile, but a small working example project, to help understand usage. 

##Task
* default // The default task (just type 'grunt' for run)
* watch // watch and build if necessary
* prod // use to deploy, do every thing: build, uglify, minify, generate favicons and inject to markup, break the cache
 
##TODO
* more css output (print.css for ex.)
* maybe useful tasks: 
  * grunt-autoprefixer
  * grunt-uncss
  * grunt-modernizr