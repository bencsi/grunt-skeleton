(function () {
    'use strict';

    init();

    function helloWorld() {
        console.log('Hello World!');
    }

    function helloGrunt() {
        console.log('Hello Grunt');
    }

    function init() {
        helloWorld();
        helloGrunt();
    }

})();